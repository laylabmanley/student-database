package Database;

import java.io.IOException;
import java.util.Scanner;

/** CommmandLineInterface class manages basic inputs and interactions with the file manager 
 *  @author COSC 311, Fall 2020, Project 1 - Layla Manley
 **/
public class CommandLineInterface {

    private Filemanager fileInterface; // Filemanager object to interact with

    /**
	 * Intializes a CLI object based upon a filemanager object
     * @param file Filemanager object that CLI interacts with
	 */
    public CommandLineInterface(Filemanager file) {
        fileInterface = file;
    }

    /**
	 * Processes a command and acts upon the filemanager object
     * @param cmd String command representing the action the user wants to take
     * @param scanner Scanner object to take in user unput
     * @throws Exception
	 */
    public boolean RunCommand(String cmd, Scanner scanner) throws Exception {
        String[] commandArgs = cmd.split(" ");
        switch (commandArgs[0].toUpperCase()) {
            case "DUMP":
                DumpData();
                break;
            case "SELECT":
                int SelIndex=Integer.parseInt(commandArgs[1]); 
                System.out.println(fileInterface.GetEntryByIndex(SelIndex).toString());
                break;
            case "DELETE":
                int DelIndex = Integer.parseInt(commandArgs[1]);
                fileInterface.LazyDelete(DelIndex);
                System.out.println("SUCCESS!");
                break;
            case "QUIT":
                return false;
            case "ADD":
                Student addStudent = GetStudentInput(scanner);
                fileInterface.AppendEntry(addStudent);
                System.out.println(String.format("Added at entry: %s", fileInterface.GetEntryCount() -1));
                break;
            case "MODIFY":
                int modIndex = Integer.parseInt(commandArgs[1]);
                Student modifyStudent = GetStudentInput(scanner);
                fileInterface.UpdateEntry(modIndex, modifyStudent);
                System.out.println(String.format("Updated at entry: %s", modIndex));
                break;
            default:
                System.out.println("Unknown command entered! Valids commands are:\n- DUMP\n- SELECT [INDEX]\n- DELETE [INDEX]\n- ADD\n- MODIFY (INDEX)\n- QUIT\n");
                break;
        }
        return true;
    }

    /**
	 * Creates a student object based upon user input
     * @param scanner Scanner object to take in user unput
     * @throws Exception
	 */
    public Student GetStudentInput(Scanner scanner) throws Exception
    {
        // Take in user input
        System.out.print("First Name: ");
        String firstName = scanner.nextLine();
        System.out.print("Last Name: ");
        String lastName = scanner.nextLine();
        System.out.print("Student ID: ");
        int studentId = scanner.nextInt();
        System.out.print("GPA: ");
        double gpa = scanner.nextDouble();
        scanner.nextLine();

        // Build and return student object
        return new Student(firstName, lastName, studentId, gpa);
    }

    /**
	 * Prints data to console
     * @throws IOException
     * @throws Exception
	 */
    public void DumpData() throws IOException, Exception{
        System.out.println(String.format("%-7s", "Index") + String.format("%-20s","First Name") + String.format("%-20s","Last Name") +  
        String.format("%-15s","SID") + String.format("%-5s","GPA"));
        for (int i = 0; i < fileInterface.GetEntryCount(); i++) {
            try {
            System.out.println(fileInterface.GetEntryByIndex(i).dumpVal(i));
            } catch (DeletedEntryException ex ) {}
        }
    }
}
