package Database;

import java.io.IOException;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.Random;
import java.util.logging.Logger;

/** Filemanager manages all file related tasks
 *  @author COSC 311, Fall 2020, Project 1 - Layla Manley
 **/
public class Filemanager {
    private final static Logger logger = Logger.getLogger(Database.class.getName()); // Logging object
    private RandomAccessFile f;
    private File file;
    
    /**
	 * Filemanager constructor generates an intial file
	 * @param fileName Name of the file to load
	 */
    public Filemanager(String fileName) throws IOException, Exception {
        
        // Intialize file object
        file = new File(fileName);

        // The file exist check is done at this point
        // because once the RAF object is created a file
        // will be created if it doesn't exist
        boolean fileExist = file.exists();

        logger.info(String.format("Loading data from: %s", fileName));

        f = new RandomAccessFile(file, "rw");
        if (!fileExist) {
            generateDefaultFile();
        }
    }

    public Filemanager(String inputFileName, String outputFileName) throws IOException, Exception {
        
        File inputFile = new File(inputFileName);

        // Intialize file object
        file = new File(outputFileName);
        
        // Replaces the output file with the input file
        file.delete();
        Files.copy(inputFile.toPath(), file.toPath());

        // The file exist check is done at this point
        // because once the RAF object is created a file
        // will be created if it doesn't exist
        boolean fileExist = file.exists();

        logger.info(String.format("Loading data from: %s", inputFileName));

        f = new RandomAccessFile(file, "rw");
        if (!fileExist) {
            generateDefaultFile();
        }
    }

	/**
	 * Writes a default student object to the RAF
	 * @throws Exception
	 */
    public void generateDefaultFile() throws Exception {
        Student defaultStudent = new Student("Example", "Student", 200, 4.0);
        Student secondStudent = new Student("Hello", "World", 9032020, 3.5);
        Student three = new Student("Java", "App", 12345, 0);
        AppendEntry(defaultStudent);
        AppendEntry(secondStudent);
        AppendEntry(three);
        f.seek(0);
    }

	/**
	 * Adds a student object to the end of file
	 * @param entry The student object that is appended to the file
	 * @throws IOException
	 */
    public void AppendEntry(Student entry) throws IOException {
        long pos = f.getFilePointer();
        f.seek(f.length());
        entry.writeToFile(f);
        f.seek(pos);
    } 

	/**
	 * Updates an entry with a new
     * @param index The numeric index of the object to update
	 * @param entry The student object that is written to the file
	 * @throws IOException
	 */
    public void UpdateEntry(int index, Student entry) throws IOException {
        long pos = f.getFilePointer();
        f.seek(index * entry.size());
        entry.writeToFile(f);
        f.seek(pos);
    } 

	/**
	 * Gets a student object by it's index in the file
	 * @param index The numeric index of the object
	 * @throws IOException
     * @throws Exception
	 */
    public Student GetEntryByIndex(int index) throws IOException, Exception {
        if(index >= GetEntryCount() || index < 0) {
            throw new Exception(String.format("Out of bounds error! Index '%s' does not exist!", GetEntryCount()));
        }
        
        // Read student object
        Student student = new Student();
        f.seek(student.size() * index);
        student.readFromFile(f);

        // Checks if the object has been "deleted"
        if (student.getFirst().replace(" ", "").contains("DELETED")) {
            throw new DeletedEntryException();
        }

        return student;
    }

	/**
	 * Returns the current amount of entries in a file
	 * @throws IOException
	 */
    public int GetEntryCount() throws IOException {
        Student student = new Student();
        return (int)(f.length() / (long)student.size());
    }

	/**
	 * Does a lazy delete on an entry
     * @param index The numeric index of the object be "deleted"
	 * @throws IOException
	 */
    public void LazyDelete(int index) throws IOException, Exception {
        UpdateEntry(index, new Student("DELETED","",0,0));
    }
    
}