package Database;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;

/** Class representing the program as a whole.
 *  @author COSC 311, Fall 2020, Project 1 - Layla Manley
 **/
public class Database {
    private static boolean StackTrace = true; // Enables stack trace for exceptions
    private final static Logger logger = Logger.getLogger(Database.class.getName()); // Logging object

    /**
	 * Main entrypoint of the database program
	 * @param args Array of arguements passed to the program
	 */
    public static void main(String[] args) {
        try {

            Filemanager file; // Filemanager object
            Scanner inputScanner = new Scanner(System.in); // Scanner used for user input

            // Load file information from arguements
            switch (args.length){
                case 0:

                    // If not data is passed generate input
                    // data from System.in

                    System.out.println("NOTE: You can pass `--help` to view how to pass a file in the cli.");
                    
                    // Read in main database file
                    System.out.print("Read/Write File [database.dat]:");
                    String mainF = inputScanner.nextLine();
                    if (mainF.length() == 0) {
                        mainF = "database.dat";
                    }

                    // Read in file to copy
                    System.out.print("Input file [NONE]:");
                    String inputF = inputScanner.nextLine();

                    // Set file object
                    if (inputF.length() > 0) {
                        file = new Filemanager(mainF, inputF);
                    }
                    else {
                        file = new Filemanager(mainF);
                    }

                    break;
                
                case 2:
                    file = new Filemanager(args[1], args[0]);
                    break;

                case 1:
                    if (args[0] != "--help" & args[0] != "-h") {
                        file = new Filemanager(args[0]);
                        break;
                    }
                default:
                    System.out.println("USAGE: java -jar Database.jar [MAINFILE] [INPUTFILE]");
                    return;
            }
            
            
            CommandLineInterface cli = new CommandLineInterface(file);

            boolean runCmd = true;
            do {
                try {

                    System.out.print("> ");

                    // Continue running until CLI returns `false`
                    runCmd = cli.RunCommand(inputScanner.nextLine(), inputScanner);

                // Catch errors related to CLI input
                } catch (IOException ex) {
                    logger.severe(String.format("An IOException Occured: %s", ex.getMessage()));
                    if (StackTrace) {
                        ex.printStackTrace();
                    }
                } catch (Exception ex) {
                    logger.severe(String.format("An Unknown Exception Occured: %s", ex.getMessage()));
                    if (StackTrace) {
                        ex.printStackTrace();
                    }
                }
            } while (runCmd);

        // Catch intialization errors
        } catch (IOException ex) {
            logger.severe(String.format("An IOException Occured: %s", ex.getMessage()));
            if (StackTrace) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            logger.severe(String.format("An Unknown Exception Occured: %s", ex.getMessage()));
            if (StackTrace) {
                ex.printStackTrace();
            }
        }
        

    }

}
