package Database;

/** Basic exception thrown when there is a lazy delete
 *  @author COSC 311, Fall 2020, Project 1 - Layla Manley
 **/
public class DeletedEntryException extends Exception {
    DeletedEntryException() {
        super("Attempted to access a deleted entry!");
    }
}
