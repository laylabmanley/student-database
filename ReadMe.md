# Database

## Classes

### Database

The Database class, located in `Database.java` holds the main entrypoint of the application. It also handles any exceptions and args passed during execution.

### Filemanager

The `Filemanager` class, located in `Filemanager.java` holds logic directly relating to interacting with the database binary file using a `RandomAccessFile` object.

### DeleteEntryException

`DeleteEntryException` is an exception class that is thrown when trying to access a file that has been "deleted" using a lazy delete.

### CommandLineInterface

The `CommandLineInterface` class manages input and interacts with the filemanager based on a string.

### Student

The `Student` class works as a single entry. The class also holds the logic for serialization.
